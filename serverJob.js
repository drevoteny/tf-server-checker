fetch("https://tf-extension-assistant.onrender.com/", {
	method: 'GET',
    headers: {
		'Content-Type': 'application/json'
    }
}).then(response => {
    if (response.message) {
      console.log(response.message);
    } else {
      console.error('No response');
    }
  })
  .catch(error => {
    console.error('ERROR!', error);
  });